﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car1 = new Car();
            car1.Make = "Oldsmobile";
            car1.Model = "Cutlas Supreme";

            Car car2 = new Car();
            car2.Make = "Geo";
            car2.Model = "Prism";

            Car car3 = new Car();
            car3.Make = "Ford";
            car3.Model = "XR6";


            Book b1 = new Book();
            b1.Author = "Robert Tabor";
            b1.Title = "Microsoft .NET XML Web Services";
            b1.ISBN = "0-000-00000-0";

            Book b2 = new Book();
            b2.Author = "Charles Dickinson";
            b2.Title = "Blah Blah";
            b2.ISBN = "1-000-00000-1";

            Book b3 = new Book();
            b3.Author = "John Doe";
            b3.Title = "Meh";
            b3.ISBN = "2-000-00000-2";

            //ArrayLists
            ArrayList carArrayList = new ArrayList();
            carArrayList.Add(car1);
            carArrayList.Add(car2);
            carArrayList.Add(car3);
            ArrayList bookArrayList = new ArrayList();
            bookArrayList.Add(b1);
            bookArrayList.Add(b2);
            bookArrayList.Add(b3);
            //Print the arrayList
            printCar(carArrayList);
            printBook(bookArrayList);
            Console.ReadLine();

            //Sorted Dictionary
            SortedDictionary<int, Car> carDict = new SortedDictionary<int, Car>();
            carDict.Add(1, car1);
            carDict.Add(2, car2);
            carDict.Add(3, car3);
            SortedDictionary<int, Book> bookDict = new SortedDictionary<int, Book>();
            bookDict.Add(1, b1);
            bookDict.Add(2, b2);
            bookDict.Add(3, b3);
            //Print the Dictionaries
            printCarDict(carDict);
            printBookDict(bookDict);
            Console.ReadLine();

            //SortedLists
            SortedList newList1 = new SortedList();
            newList1.Add(1, car1);
            newList1.Add(2, car2);
            newList1.Add(3, car3);
            SortedList newList2 = new SortedList();
            newList2.Add(1, b1);
            newList2.Add(2, b2);
            newList2.Add(3, b3);
            //Print The SortedLists
            printList(newList1);
            printList(newList2);
            Console.ReadLine();

            
        }
        static void printCar(ArrayList a)
        {
            foreach (Car i in a)
                Console.WriteLine(i.ToString());
        }

        static void printBook(ArrayList a)
        {
            foreach (Book i in a)
                Console.WriteLine(i.ToString());
        }

        static void printCarDict(SortedDictionary<int, Car> a)
        {
            foreach (KeyValuePair<int, Car> pair in a)
                Console.WriteLine(pair.ToString());
        }

        static void printBookDict(SortedDictionary<int, Book> a)
        {
            foreach (KeyValuePair<int, Book> pair in a)
                Console.WriteLine(pair.ToString());
        }

        static void printList(SortedList a)
        {
            ICollection key = a.Keys;
            foreach (int k in key)
                Console.WriteLine(a[k].ToString());
        }

    }

    class Car
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public override string ToString()
        {
            return Make + "   " + Model;
        }
    }


    class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }
        public override string ToString()
        {
            return Title + "   " + Author + "  " + ISBN;
        }
    }

}
